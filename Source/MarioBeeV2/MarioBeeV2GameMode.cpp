// Copyright Epic Games, Inc. All Rights Reserved.

#include "MarioBeeV2GameMode.h"
#include "MarioBeeV2Character.h"
#include "UObject/ConstructorHelpers.h"

AMarioBeeV2GameMode::AMarioBeeV2GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
