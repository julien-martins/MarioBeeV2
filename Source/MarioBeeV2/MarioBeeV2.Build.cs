// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MarioBeeV2 : ModuleRules
{
	public MarioBeeV2(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
