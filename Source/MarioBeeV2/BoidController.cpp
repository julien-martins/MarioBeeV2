// Fill out your copyright notice in the Description page of Project Settings.


#include "BoidController.h"

#include <string>

#include "Components/InstancedStaticMeshComponent.h"
#include "Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ABoidController::ABoidController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	_state = BoidState::ROAM;
}

// Called when the game starts or when spawned
void ABoidController::BeginPlay()
{
	Super::BeginPlay();

	BoidInstanceMesh = NewObject<UInstancedStaticMeshComponent>(this);
	BoidInstanceMesh->RegisterComponent();
	BoidInstanceMesh->SetStaticMesh(boidMesh);
	BoidInstanceMesh->SetGenerateOverlapEvents(false);
	BoidInstanceMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	BoidInstanceMesh->CastShadow = false;

	centerPos = GetActorLocation();

	FVector4 pos;
	FRotator rot;
	TArray<FVector4> emptyArray;
	
	for(int i = 0; i < boidNumber; ++i) {
		pos = FVector4(
				((float(FMath::RandRange(-1000, 1000)) / 1000.0f) * boidsRangeX) + centerPos.X,
				((float(FMath::RandRange(-1000, 1000)) / 1000.0f) * boidsRangeY) + centerPos.Y,
				((float(FMath::RandRange(-1000, 1000)) / 1000.0f) * boidsRangeZ) + centerPos.Z,
				i
		);

		rot = FRotator(
				float(FMath::RandRange(-180, 180)),
				float(FMath::RandRange(-180, 180)),
				float(FMath::RandRange(-180, 180))
		);

		neightbors.Push(emptyArray);

		BoidInstanceMesh->AddInstance(FTransform(rot, pos, FVector(boidScale, boidScale, boidScale)), true);
		boidPos.Push(pos);
		boidsTransformsArray.Push(FTransform(rot, pos, FVector(boidScale, boidScale, boidScale)));
	}

	_target = UGameplayStatics::GetActorOfClass(GetWorld(), ATargetPoint::StaticClass());

}

// Called every frame
void ABoidController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//DrawDebugBox(GetWorld(), GetActorLocation() + FVector(), FVector(boidsRangeX, boidsRangeY, boidsRangeZ), FColor::Blue);

	FTransform tmp;

	ParallelFor(boidNumber, [&](int32 i)
		{
			getNeightbors(i, boidPos[i]);
		});

	for(int i = 0; i < boidNumber; ++i)
	{
		BoidInstanceMesh->GetInstanceTransform(boidPos[i].W, tmp);
		FVector newDirection = tmp.GetRotation().GetForwardVector();

		cohesion(boidPos[i].W, newDirection, boidPos[i]);
		alignment(boidPos[i].W, newDirection, tmp.GetRotation().GetForwardVector());
		separation(boidPos[i].W, newDirection, boidPos[i]);

		if (_state == BoidState::CHASE) {
			if (IsValid(_target)) {
				moveTowardTarget(newDirection, tmp.GetLocation());
			}
		} else if (_state == BoidState::EVADE) {
			if (IsValid(_target) && activateRepulsion) {
				repulsion(boidPos[i].W, newDirection, boidPos[i]);
			}
		}

		keepInBounds(newDirection, tmp.GetLocation());
		limitSpeed(newDirection);

		//Calculate new rotation and position
		auto newRot = UKismetMathLibrary::FindLookAtRotation(tmp.GetLocation(), tmp.GetLocation() + (newDirection * 10.0f));
		boidsTransformsArray[boidPos[i].W] = FTransform(FMath::Lerp(tmp.GetRotation(), newRot.Quaternion(), DeltaTime * boidSpeed),  tmp.GetLocation() + (newDirection * 20), tmp.GetScale3D());
		boidPos[i] = FVector4(tmp.GetLocation() + (newDirection * 20), boidPos[i].W);
	}

	BoidInstanceMesh->BatchUpdateInstancesTransforms(0, boidsTransformsArray, true, true, false);
}

void ABoidController::randomizeDirection() {

	FRotator rot;

	for (int i = 0; i < boidNumber; ++i) {

		rot = FRotator(
			float(FMath::RandRange(-180, 180)),
			float(FMath::RandRange(-180, 180)),
			float(FMath::RandRange(-180, 180))
		);

		boidsTransformsArray[i] = FTransform(
									rot, 
									boidsTransformsArray[i].GetLocation(), 
									FVector(boidScale, boidScale, boidScale));
	}
}

void ABoidController::keepInBounds(FVector& dir, FVector4 pos)
{
	if(pos.X < centerPos.X - boidsRangeX)
		dir.X = dir.X + 0.5f;
	if (pos.X > centerPos.X + boidsRangeX)
		dir.X = dir.X - 0.5f;

	if (pos.Y < centerPos.Y - boidsRangeY)
		dir.Y = dir.Y + 0.5f;
	if (pos.Y > centerPos.Y + boidsRangeY)
		dir.Y = dir.Y - 0.5f;

	if (pos.Z < centerPos.Z - boidsRangeZ)
		dir.Z = dir.Z + 0.5f;
	if (pos.Z > centerPos.Z + boidsRangeZ)
		dir.Z = dir.Z - 0.5f;
}

void ABoidController::limitSpeed(FVector& dir)
{
	float speed = FMath::Sqrt(dir.X * dir.X + dir.Y * dir.Y + dir.Z * dir.Z);
	if(speed > speedLimit) {
		dir.X = (dir.X / speed) * speedLimit;
		dir.Y = (dir.Y / speed) * speedLimit;
		dir.Z = (dir.Z / speed) * speedLimit;
	}
}

void ABoidController::getNeightbors(int boidIndex, FVector loc)
{
	neightbors[boidIndex].Empty();
	for(int i = 0; i < boidNumber; ++i)
	{
		if(i != boidIndex)
		{
			if(FVector::Dist(boidPos[i], loc) < visualRange)
			{
				neightbors[boidIndex].Push(boidPos[i]);
			}
		}
	}
	//UE_LOG(LogTemp, Warning, TEXT("%i"), neightbors[boidIndex].Num() );

}

void ABoidController::separation(int boidIndex, FVector& dir, FVector loc)
{
	FVector newDir = FVector(0, 0, 0);

	for(int i = 0; i < neightbors[boidIndex].Num(); ++i)
	{
		if(FVector::Dist(loc, neightbors[boidIndex][i]) < separationRange)
		{
			newDir.X += loc.X - neightbors[boidIndex][i].X;
			newDir.Y += loc.Y - neightbors[boidIndex][i].Y;
			newDir.Z += loc.Z - neightbors[boidIndex][i].Z;
		}
	}

	dir.X += newDir.X * separationCoeff;
	dir.Y += newDir.Y * separationCoeff;
	dir.Z += newDir.Z * separationCoeff;
}

void ABoidController::cohesion(int boidIndex, FVector& dir, FVector loc)
{
	FVector centerMass = FVector(0, 0, 0);

	for(int i = 0; i < neightbors[boidIndex].Num(); ++i)
	{
		centerMass.X += neightbors[boidIndex][i].X;
		centerMass.Y += neightbors[boidIndex][i].Y;
		centerMass.Z += neightbors[boidIndex][i].Z;
	}

	auto neightborsCount = neightbors[boidIndex].Num();
	if(neightborsCount > 0)
	{
		centerMass = FVector(centerMass.X / neightborsCount, centerMass.Y / neightborsCount, centerMass.Z / neightborsCount);
		dir.X += ((centerMass.X - loc.X) * cohesionCoeff);
		dir.Y += ((centerMass.Y - loc.Y) * cohesionCoeff);
		dir.Z += ((centerMass.Z - loc.Z) * cohesionCoeff);
	}

}

void ABoidController::alignment(int boidIndex, FVector& dir, FVector startDir)
{
	FVector averageDirection = FVector(0, 0, 0);
	FTransform neightborsTransform;
	FVector forwardVector;

	auto neightborsCount = neightbors[boidIndex].Num();

	for(int i = 0; i < neightborsCount; ++i)
	{
		BoidInstanceMesh->GetInstanceTransform(neightbors[boidIndex][i].W, neightborsTransform);
		forwardVector = neightborsTransform.GetRotation().GetForwardVector();
		averageDirection.X += forwardVector.X;
		averageDirection.Y += forwardVector.Y;
		averageDirection.Z += forwardVector.Z;
	}

	if(neightborsCount > 0)
	{
		averageDirection = FVector(averageDirection.X / neightborsCount, averageDirection.Y / neightborsCount, averageDirection.Z / neightborsCount);
		dir.X += ((averageDirection.X - startDir.X) * alignmentCoeff);
		dir.Y += ((averageDirection.Y - startDir.Y) * alignmentCoeff);
		dir.Z += ((averageDirection.Z - startDir.Z) * alignmentCoeff);
	}

}

void ABoidController::moveTowardTarget(FVector& dir, FVector loc)
{
	FVector targetPos = _target->GetActorLocation();

	dir.X += (targetPos.X - loc.X) * 0.05f;
	dir.Y += (targetPos.Y - loc.Y) * 0.05f;
	dir.Z += (targetPos.Z - loc.Z) * 0.05f;
}

void ABoidController::SetTarget(AActor* actor)
{
	_target = actor;
}

void ABoidController::UnTarget()
{
	_target = nullptr;
}

void ABoidController::repulsion(int boidIndex, FVector& dir, FVector loc) {

	FVector newDir = FVector(0, 0, 0);

	auto neightborsCount = neightbors[boidIndex].Num();
	auto targetLoc = _target->GetActorLocation();

	if (FVector::Dist(loc, targetLoc) < repulsionRadius)
	{
		newDir.X += loc.X - targetLoc.X;
		newDir.Y += loc.Y - targetLoc.Y;
		newDir.Z += loc.Z - targetLoc.Z;
	}

	dir.X += newDir.X * avoidmentCoeff;
	dir.Y += newDir.Y * avoidmentCoeff;
	dir.Z += newDir.Z * avoidmentCoeff;
}

void ABoidController::ChangeState(BoidState state) {
	_state = state;
	/*
	if (_state == BoidState::ROAM) {
		randomizeDirection();
	}
	*/
}
