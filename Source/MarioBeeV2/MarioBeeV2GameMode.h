// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MarioBeeV2GameMode.generated.h"

UCLASS(minimalapi)
class AMarioBeeV2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMarioBeeV2GameMode();
};



