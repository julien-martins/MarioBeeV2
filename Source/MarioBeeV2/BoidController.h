// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SplineComponent.h"
#include "GameFramework/Actor.h"
#include "BoidController.generated.h"

UENUM(BlueprintType)
enum BoidState {
	ROAM UMETA(DisplayName = "ROAM"),
	CHASE UMETA(DisplayName = "CHASE"),
	EVADE UMETA(DisplayName = "EVADE")
};

UCLASS()
class MARIOBEEV2_API ABoidController : public AActor
{
	GENERATED_BODY()
		
public:	
	
	// Sets default values for this actor's properties
	ABoidController();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		class UStaticMesh* boidMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		int  boidNumber = 1000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		int  boidsRangeX = 2000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		int  boidsRangeY = 2000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		int  boidsRangeZ = 2000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		float  boidScale = 0.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		float  speedLimit = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		float  visualRange = 150;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		float  separationRange = 80;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		float boidSpeed = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Rules Coeff")
		float  separationCoeff = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Rules Coeff")
		float  cohesionCoeff = 0.001f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Rules Coeff")
		float  alignmentCoeff = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Rules Coeff")
		float avoidmentCoeff = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		bool activateRepulsion = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Boid Controller")
		float repulsionRadius = 1.0f;

	UFUNCTION(BlueprintCallable)
	void SetTarget(AActor* actor);

	UFUNCTION(BlueprintCallable)
	void UnTarget();

	UFUNCTION(BlueprintCallable)
		void ChangeState(BoidState state);

	FVector centerPos;

	UInstancedStaticMeshComponent* BoidInstanceMesh;

	TArray<FTransform> boidsTransformsArray;
	TArray<FVector4> boidPos;

	TArray<TArray<FVector4>> neightbors;

	BoidState _state;

	AActor* _target;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void keepInBounds(FVector& dir, FVector4 pos);
	void limitSpeed(FVector& dir);

	void getNeightbors(int boidIndex, FVector loc);

	//Rules
	void separation(int boidIndex, FVector &dir, FVector loc);
	void cohesion(int boidIndex, FVector &dir, FVector loc);
	void alignment(int boidIndex, FVector& dir, FVector startDir);

	void repulsion(int boidIndex, FVector &dir, FVector loc);

	void randomizeDirection();

	void moveTowardTarget(FVector &dir, FVector loc);

};
